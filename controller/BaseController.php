<?php 

/**
 * Base controller that will be the base for all other controllers in the application.
 * Main responsibility is to control what processes are to be ran or displayed.
 * This class uses corresponding Models as well as the includes of Input, Response, and Session.
 * 
 * @author Keith Andrews
 */
class BaseController {
	
	protected $pk;
	protected $process;
	protected $model;
	protected $response;
	protected $mailer;
	protected $redirectLink = '';

	/**
	 * Base controller constructor
     */
	public function __construct(){
		// Set the Request vars to use in the class
		$this->pk      = Input::get('pk');
		$this->process = Input::get('process');

		// Init the response class in order to show notices
		$this->response = (Session::varIsset('Response')) ? Session::getVar('Response') : new Response();
		// Init the mailer to be able to send out notifications
		$this->mailer   = new Mailer();
    }
	
	/**
	 * Dispatch the action to the correct method
	 */
	public function dispatch(){
        $process = ($this->process) ? $this->process : 'view';		
		$this->$process();
    }

	
	/** 
	 * Redirect the page via the Wordpress redirect method or javascript if the headers are already sent
	 * Stores the response in the session for later use
	 * @param string $link
	 */
	protected function pageRedirect($link = NULL){
		if($this->response->getHasNotice())
			Session::setVar('Response', $this->response);
			
		$url = ($link) ? $link : $this->redirectLink;
		if (headers_sent())
			echo '<script>location.replace("' . $url . '"); </script>';
		else
			\wp_redirect($url);
		exit;
	}
	
	/**
	 * Create the option HTML for a select input
	 * @param mixed $key
	 * @param mixed|array $value
	 * @param array $list
	 * @return string
	 */
	public static function createOptionArray($key, $value, $list){
		$options = array('' => '-- Select --');
		$display = '';
		
		// Loop through the list and add to the options array
		if(!empty($list)){
			foreach($list as $item){
				// If the $value was passed as an array, combine the text
				if(is_array($value)){
					foreach($value as $col){
						$display .= $item->$col . ' ';
					}
					$display = trim($display);
				} else
					$display = $item->$value;
				
				// Set the option key to the $key and $display
				$options[$item->$key] = $display;
			}			
		}
		
		return $options;
	}
}
?>