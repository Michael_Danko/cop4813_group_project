<?php

/**
 * Project: cop4813_group_project
 * Author: Keith
 */
class UserController extends BaseController {

	private $user;

	/**
	 * Controller constructor
	 */
	public function __construct(){
		parent::__construct();

		$this->model = new User();
		self:setUser();
	}

	/**
	 * Display the login form
	 * @return string
	 */
	public function loginView(){
		return View::make('forms/login.php');
	}

	/**
	 * Display the user's data during login
	 * @return string
	 */
	public function userDataView(){
		return View::make('misc/user-data.php', array('user' => $this->user));
	}

	/**
	 * Validate and check the credentials entered for logging the user into the system
	 * @param array $data
	 * @return bool|string
	 * @throws ErrorException
	 */
	public function login($data = array()){
		$response = false;

		foreach($data as $index => $entry){
			switch($entry['name']){
				case 'PASSWORD':
					$password = $entry['value'];
					break;
				case 'USERNAME':
					$username = $entry['value'];
					$object   = $this->model->getUserByUsername($username);
					if(empty($object))
						throw new ErrorException('Invalid credentials entered.');
					break;
			}
		}

		// Make sure both are valid
		if(!isset($username) || !isset($password))
			throw new ErrorException('Please fill in all fields in order to login.');

		// Check the password and if valid set the session data
		if(Input::passwordVerify($password, $object->PASSWORD)){
			Session::setVar('USER_ID', $object->USER_ID);
			$response = 'Login successful! Redirecting...';
		} else
			throw new ErrorException('Invalid credentials entered.');

		return $response;
	}

	/**
	 * Logout the user by destroying the session
	 * @return string
	 */
	public function logout(){
		Session::end();

		return 'Session has been ended.';
	}

	/**
	 * Set the user model base on the USER_ID in the session
	 */
	public function setUser(){
		$this->user = (Session::varIsset('USER_ID')) ? $this->model->find(Session::getVar('USER_ID')) : NULL;
	}
}