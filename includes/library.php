<?php
/**
 * Project: cop4813_group_project
 * Author: Keith
 */

require 'config.php';

include 'input.php';
include 'mailer.php';
include 'session.php';

include '../view/view.php';

include '../model/BaseModel.php';
include '../model/User.php';

include '../controller/BaseController.php';
include '../controller/UserController.php';