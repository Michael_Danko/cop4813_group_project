<!doctype html>
<!--[if IE 9]>
	<html class="lt-ie10" lang="en">
<![endif]-->
<html lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
		<meta http-equiv="pragma" content="no-cache"/>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Shop</title>
		<link rel="stylesheet" href="../assets/css/site.css"/>
		<script src="./assets/css/jquery/jquery-ui.min.css.min.js"></script>
		<script src="./assets/js/jquery/jquery.min.js"></script>
		<script src="./assets/js/jquery/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="./assets/css/font-awesome.min.css"/>
	</head>

	<body>
		<div class="row">
			<div class="large-12 columns">
				<div class="nav-bar right">
					<ul class="button-group">
						<li><a href="../index.php" class="button">Home</a></li>
						<li><a href="../cart.php" class="button">Cart</a></li>
						<li><a href="../team.php" class="button">Team</a></li>
					</ul>
				</div>
				<h1>Shop</h1>
				<hr/>
			</div>
		</div>

