<?php
namespace Pond\SubconPortal\Includes;

use Pond\SubconPortal\Includes\Session as Session;
use Pond\SubconPortal\Views\View as View;

/**
 * Response class that stores data to be passed to the user. 
 * This class uses View and Session.
 */
class Response {
	
	private $response;
	private $status;
	private $hasNotice;
	
	/**
	 * Set the class vars to the default values
	 */
	public function __construct() {

    } 
	
	/** 
	 * Set all of the class vars in one swoop
	 * @param mixed $response
	 * @param string $status
	 */
	public function set($response, $status = 'updated'){
		$this->flush();
		
		$this->setResponse($response);
		$this->setStatus($status);
		$this->setHasNotice(true);	
	}
	
	/** 
	 * Set all of the class vars in one swoop for displaying an error
	 * @param mixed $response
	 * @param string $status
	 */
	public function error($response){
		$this->flush();
		
		$this->setResponse($response);
		$this->setStatus('error');
		$this->setHasNotice(true);	
	}
	
	/**
	 * Set the response message
	 * @param mixed $response
	 */
	public function setResponse($response){
		$this->response = $response;
	}
	
	/**
	 * Set the status of the response
	 * @param string $status
	 */
	public function setStatus($status){
		$this->status = $status;
	}
	
	/**
	 * Set if the response has a notice
	 * @param bool $notice
	 */
	public function setHasNotice($notice = false){
		$this->hasNotice = (bool)$notice;
	}
	
	/**
	 * Get the value for the hasNotice variable
	 * @return bool
	 */
	public function getHasNotice(){
		return $this->hasNotice;
	}
	
	/**
	 * Action that will display the response
	 */
	public function adminNoticeAction(){
		if($this->hasNotice){
			View::make('misc/admin-response.php', array('status' => $this->status, 'response' => $this->response));
			
			// Remove the session var, just in case
			self::flush('Response');
		}
		
		return;
	}
	
	/**
	 * Flush the class vars to the default values
	 */
	public function flush(){
		$this->setResponse('');
		$this->setStatus('');
		$this->setHasNotice(false);
	}	
	
	/**
	 * Flush the class vars to the default values
	 */
	public function setInSession(){
		self::setVar('response', $this);
		$this->setResponse('');
		$this->setStatus('');
		$this->setHasNotice(false);
	}	
}