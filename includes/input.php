<?php
/**
 * Input class which controls the data that is passed via the $_REQUEST, $_POST, and $_GET variables.
 * All data sanitizing is handled through this class, so no need to add any in other classes.
 * 
 * @author Keith Andrews
 */
class Input {
	
	public function __construct() {

    }
	
	/**
	 * Get the URL parameter or return the a default value if not set
	 * @param mixed $key
	 * @param mixed $default
	 * @return mixed
	 */
	public static function get($key, $default = NULL){
		if(self::has($key)){
			if(!is_array($_REQUEST[$key]))
				$data = htmlentities($_REQUEST[$key]);
			else
				$data = $_REQUEST[$key];
		} else 
			$data = $default;
			
		return $data;	
	}
	
	/**
	 * Get all of the URL parameters
	 * @return array
	 */
	public static function all(){
		$data = array();
		if(count($_REQUEST) > 0){
			foreach($_REQUEST as $key => $value){
				if(!is_array($value))
					$data[$key] = htmlentities($value);
				else
					$data[$key] = $value;
			}
		}
		return $data;	
	}
	
	/**
	 * Check to see if the URL parameter is set
	 * @param mixed $key
	 * @return bool
	 */
	public static function has($key){
		return isset($_REQUEST[$key]);	
	}
	
	/**
	 * Modify the password for database storage using md5
	 * @param mixed $pw
	 * @return string
	 */
	public static function passwordEncrypt($pw){
		$hashed = md5($pw);
		
		return $hashed;
	}
	
	/**
	 * Verify the password matches the hashed password
	 * @param mixed $pw
	 * @param mixed $match
	 * @return bool
	 */
	public static function passwordVerify($pw, $match){
		return (md5($pw) == $match) ? true : false;
	}
}
?>