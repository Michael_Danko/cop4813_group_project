<?php

/**
 * SQL class which is responsible for creating and running SQL scripts on the database.
 * Most methods are built with the ability to chain together multiple methods.
 * For a more generic build, this class uses data from the Model to build scripts.
 * This class uses Models.
 * 
 * @author Keith Andrews
 */
class SQL {
	private $sql;
	private $model;
	private $error;
	private $mysqli;

	/**
	 * SQL construction method to create the connection to the database
	 * @param $model
	 */
	public function __construct($model) {
		self::setMYSQLI();
		$this->model = $model;
        $this->sql   = "";
    }

	/**
	 * Set the mysqli to the MYSQL instance
	 */
	private function setMYSQLI(){
		$this->mysqli = new mysqli(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DB);
		if($this->mysqli->connect_error)
			$this->error = 'Failed to connect to MySQL: (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error;
	}

	/**
	 * Set the sql variable to the Model's columns and from table
	 * @return instance $this
	 */
	public function select() {
        $this->sql = "SELECT " . $this->model->getColumnsForSQL() . " FROM " . $this->model->getTable() . " ";
		
		return $this;
    }
	
	/**
	 * Add a "AND" condition to the SQL var
	 * @param string $col
	 * @param string $expression
	 * @param mixed  $value
	 * @return instance $this
	 */
	public function where($col, $expression, $value) {
		$this->appendWhere();
        $this->sql .= $this->model->getTable() . "." . $col . " ";
		$this->conditionCreate($expression, $value);
		
		return $this;
    }
	
	/**
	 * Add a "OR" condition to the SQL var
	 * @param string $col
	 * @param string $expression
	 * @param mixed  $value
	 * @return instance $this
	 */
	public function orWhere($col, $expression, $value) {
		$this->appendWhere(false);
        $this->sql .= $this->model->getTable() . "." . $col . " ";
		$this->conditionCreate($expression, $value);
		
		return $this;
    }
	
	/**
	 * Create the condition based on the expression passed
	 * @param string $expression
	 * @param mixed  $value
	 */
	private function conditionCreate($expression, $value){
		switch($expression){
			case 'IN':
			case 'in':
				$value = (is_array($value)) ? implode("','", $value) : $value;
				$this->sql .= $expression . " ('" . $value . "')";
				break;
			case 'LIKE':
			case 'like':
			case '=':
			case '<>':
			case '>':
			case '<':
			default:
				$this->sql .= $expression . " '" . $value . "'";
				break;
		}
		$this->sql .= " ";
		
	}
	
	/**
	 * Method to decide if a condition "AND"/"OR" should be added or a "WHERE"  to the SQL var
	 * @param bool $isAnd
	 */
	private function appendWhere($isAnd = true){
		if(strpos($this->sql, 'WHERE') === false)
			$this->sql .= "WHERE ";
		else {
			if($isAnd)	
				$this->sql .= "AND ";
			else
				$this->sql .= "OR ";
		}
	}
	
	/** 
	 * Join a table onto the the sql variable
	 * @param string $model
	 * @param string $expression
	 * @param string $baseCol
	 * @return $this
	 */
	public function join($model, $expression, $baseCol){		
		$attach = new $model();
		
		// Add the joined table's columns to the select
		$this->appendJoinCols($attach);
		
		$this->sql .= "JOIN " . $attach->getTable() . " ON " . $attach->getTable() . "." . $attach->getPrimaryKey() . " " . $expression . " " . $baseCol . " ";
		
		return $this;
	}
	
	/** 
	 * Left join a table onto the the sql variable
	 * @param string $model
	 * @param string $expression
	 * @param string $baseCol
	 * @return $this
	 */
	public function leftJoin($model, $expression, $baseCol){
		$attach = new $model();
		
		// Add the joined table's columns to the select
		$this->appendJoinCols($attach);
		
		$this->sql .= "LEFT JOIN " . $attach->getTable() . " ON " . $attach->getTable() . "." . $attach->getPrimaryKey() . " " . $expression . " " . $baseCol . " ";
		
		return $this;
	}
	
	/** 
	 * Right join a table onto the the sql variable
	 * @param string $model
	 * @param string $expression
	 * @param string $baseCol
	 * @return $this
	 */
	public function rightJoin($model, $expression, $baseCol){
		$attach = new $model();
		
		// Add the joined table's columns to the select
		$this->appendJoinCols($attach);
		
		$this->sql .= "RIGHT JOIN " . $attach->getTable() . " ON " . $attach->getTable() . "." . $attach->getPrimaryKey() . " " . $expression . " " . $baseCol . " ";
		
		return $this;
	}
	
	/** 
	 * Inner join a table onto the the sql variable
	 * @param string $model
	 * @param string $expression
	 * @param string $baseCol
	 * @return $this
	 */
	public function innerJoin($model, $expression, $baseCol){
		$attach = new $model();
		
		// Add the joined table's columns to the select
		$this->appendJoinCols($attach);
		
		$this->sql .= "INNER JOIN " . $attach->getTable() . " ON " . $attach->getTable() . "." . $attach->getPrimaryKey() . " " . $expression . " " . $baseCol . " ";
		
		return $this;
	}

	/**
	 * Full join a table onto the the sql variable
	 * @param string $model
	 * @param string $expression
	 * @param string $baseCol
	 * @return $this
	 */
	public function fullJoin($model, $expression, $baseCol){
		$attach = new $model();
		
		// Add the joined table's columns to the select
		$this->appendJoinCols($attach);
		
		$this->sql .= "FULL JOIN " . $attach->getTable() . " ON " . $attach->getTable() . "." . $attach->getPrimaryKey() . " " . $expression . " " . $baseCol . " ";
		
		return $this;
	}

	/**
	 * Order the query
	 * @param string $order
	 * @param string $direction
	 * @return $this
	 */
	public function orderBy($order, $direction){
		if(strpos($this->sql, 'ORDER BY') === false)
			$this->sql .= "ORDER BY ";

		$this->sql .= $order . " " . $direction . " ";
		
		return $this;
	}
	
	/**
	 * Get the results of the sql variable with no limit
	 * @return array
	 */
	public function get(){
		// Add the default orderby if nothing has been called
		if(strpos($this->sql, 'ORDER BY') === false)
			$this->orderBy($this->model->sortBy, $this->model->sortDir);
			
		$results = $this->mysqli->query($this->sql);
		$this->setLastError();

		return $results;
	}

	/**
	 * Get a single row based off of the primary key
	 * @param mixed|array $value
	 * @return object
	 * @throws \ErrorException
	 */
	public function find($value){
		$condition = "WHERE ";

		// Check if there is a composite key for the primary key
		if(is_array($this->model->getPrimaryKey()) && is_array($value)){

			$lastKey = end($this->model->getPrimaryKey());
			foreach($this->model->getPrimaryKey() as $key){
				$condition .= $key . " = '" . $value[$key] . "'";

				// Append logic if not at the end
				if($lastKey != $key)
					$condition .= " AND ";
			}

		} elseif(is_array($this->model->getPrimaryKey()) && !is_array($value))
			throw new ErrorException('In order to search a model with a composite primary key, the value passed must be an array.', 1000);
		else
			$condition .= $this->model->getPrimaryKey() . " = '" . $value . "'";

		// Run the query
		$object = $this->mysqli->get_row("SELECT * FROM " . $this->model->getTable() . " " . $condition);

		$this->setLastError();

		return $object;
	}
	
	/**
	 * Get a the first row of a search based off of the SQL var
	 * @return object
	 */
	public function findFirst(){
		// Add the default orderby if nothing has been called
		if(strpos($this->sql, 'ORDER BY') === false)
			$this->orderBy($this->model->sortBy, $this->model->sortDir);
			
		$object = $this->mysqli->get_row($this->sql);
		$this->setLastError();
		
		return $object;
	}
	
	/**
	 * Get the count of the number of rows in the Model
	 * @return int
	 */
	public function count(){
		$this->sql = "SELECT COUNT(" . $this->model->getTable() . "." . $this->model->getPrimaryKey() . ") " . strstr($this->sql, 'FROM');
		$this->setLastError();
		return $this->mysqli->query($this->sql);
	}
	
	/**
	 * Delete a single or many rows based off of the primary key
	 * @param string|int|array $value 
	 * @return bool
	 */
	public function delete($value){
		if(is_array($value))
			$result = $this->mysqli->query("DELETE FROM " . $this->model->getTable() . " WHERE " . $this->model->getPrimaryKey() . " IN ('" . implode("','", $value) . "')");
		else
			$result = $this->mysqli->delete($this->model->getTable(), array($this->model->getPrimaryKey() => $value));
		
		$this->setLastError();
		
		return $result;
	}
	
	/**
	 * Insert new data into the model's table
	 * @return bool|int
	 */
	public function insert(){
		$results = $this->mysqli->insert(
			$this->model->getTable(),
			$this->model->getAttributes()
		);

		if($results && $this->model->autoIncrement)
			$results = $this->mysqli->insert_id;
		
		$this->setLastError();

		return $results;
	}
	
	/**
	 * Update existing data in the model's table
	 * @return bool|int
	 */
	public function update(){
		$condition = array();

		// Check for primary key as array
		if(is_array($this->model->getPrimaryKey())){
			foreach($this->model->getPrimaryKey() as $key){
				$condition[$key] = $this->model->getAttribute($key);
			}
		} else
			$condition[$this->model->getPrimaryKey()] = $this->model->getAttribute($this->model->getPrimaryKey());

		// Run the query
		$results = $this->mysqli->update(
			$this->model->getTable(),
			$this->model->getAttributesNoPK(),
			$condition
		);
		
		$this->setLastError();
		
		return $results;
	}

	/**
	 * Update specific data in the model's table using passed values of an associative array
	 * @param array $values
	 * @return bool|int
	 * @throws \ErrorException
	 */
	public function updateSpecific($values = array()){
		if(empty($values))
			throw new \ErrorException('Cannot update row with no data set.', 4009);

		$condition = array();

		// Check for primary key as array
		if(is_array($this->model->getPrimaryKey())){
			foreach($this->model->getPrimaryKey() as $key){
				$condition[$key] = $this->model->getAttribute($key);
			}
		} else
			$condition[$this->model->getPrimaryKey()] = $this->model->getAttribute($this->model->getPrimaryKey());

		// Run the query
		$results = $this->mysqli->update(
			$this->model->getTable(),
			$values,
			$condition
		);

		$this->setLastError();

		return $results;
	}

	/**
	 * Get the table's columns
	 * @return array
	 */
	public function cols(){
		$results = $this->mysqli->get_col("DESC " . $this->model->getTable(), 0);
		$this->setLastError();
		
		return $results;
	}
	
	/** 
	 * Get the last error that happened in the database
	 * @return string
	 */
	public function setLastError(){
		if($this->mysqli->error){
			$this->error  = $this->mysqli->error;
			$this->error .= ' (' . $this->mysqli->info . ')';
		} else
			$this->error = NULL;
	}
	
	/** 
	 * Get the last error that happened in the database as well as the sql script
	 * @return string
	 */
	public function getLastError(){
		return $this->error;
	}
	
	/**
	 * Append the cols from a joined table into the sql var
	 * @param object $model
	 */
	private function appendJoinCols($model){
		$find    = ' FROM';
		$replace = ',' . $model->getColumnsForSQL() . ' FROM';
		
		$this->sql = str_replace($find, $replace, $this->sql); 
	}
}
?>	