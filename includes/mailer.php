<?php

/**
 * Mailer class which controls usage of sending mail in the plugin
 * 
 * @author Keith Andrews
 */
class Mailer {


	public function __construct() {
    }

	/**
	 * Send an email from the system to one or many emails, along with attachments
	 * @param string|array $to
	 * @param string       $subject
	 * @param string       $message
	 * @param string       $extraHeaders
	 * @param string|array $attachments
	 * @return bool
	 * @throws \ErrorException
     */
	public function send($to, $subject = '', $message = '', $extraHeaders = '', $attachments = ''){
		$from     = 'no_reply@unf.edu';
		$siteName = SITE_NAME;

		// Setup email to be sent
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: " . $siteName . "<" . $from . ">" . "\r\n";
		$headers .= $extraHeaders;

		$content = $message;
		$content .= "<br/><br/>Thank you," . "<br/>";
		$content .= $siteName . ' Support Team';
		$content .= "<br/><br/>";
		$content .= "---------- Automated Email DO NOT REPLY ----------";

		// Parse the to emails or throw exception if there isn't one
		$sendTo = $this->parseTo($to);
		if(!$sendTo)
			throw new \ErrorException('Could not send email since there is no email to send to.', 1500);

		// Fire that baby out
		$sent = wp_mail($sendTo, $subject, $content, $headers, $attachments);

		return $sent;
	}

	/**
	 * Parse the email sting or array for delivery
	 * @param $to
	 * @return array|bool|string
     */
	private function parseTo($to){
		$sendTo = '';

		switch($to){
			case '':
				$sendTo = false;
				break;
			case is_array($to):
				$sendTo = implode(',', $to);
				break;
			default:
				$sendTo = $to;
				break;
		}

		return $sendTo;
	}
}
?>