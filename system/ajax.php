<?php
include '../includes/library.php';

ob_start();

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']))
	exit('Direct access is not allowed, so stop it.');
	
require $_SERVER['DOCUMENT_ROOT'] . "/mars/src/common/CodeBase.php"; 

// Module and Method required, in some type of way
$module  = @$_REQUEST['mod'];
$method  = @$_REQUEST['meth'];
$showRaw = @$_REQUEST['_raw'];

// Lets try to process this bad boy
try{
	if(!isset($module))
		throw new ErrorException('Invalid module set.', 0);
	else if(!isset($method))
		throw new ErrorException('Invalid method set.', 1);
	
	// Set the controller			
	$classController = $module . 'Controller'; 
	
	// Make sure the class and method actually exist   
	if(!class_exists($classController))
		throw new ErrorException('Class does not exist.', 2);
	else if(!method_exists ($classController, $method))
		throw new ErrorException('Method does not exist.', 3);
	
	// Where the magic happens
	$controller = new $classController();
	$data       = $controller->$method();
	
	// Put the response into an array for formatting
	$response   = array(
		'status'  => 'success',
		'message' => $data
	);
	$error = false;	
} catch(Exception $e){
	$response = array(
		'status' => ($e->getCode() >= 2000 && $e->getCode() < 3000) ? 'success' : 'error', // Responses in the 2000s get a success status
		'code'    => $e->getCode(),
		'line'    => $e->getLine(),
		'message' => $e->getMessage()
	);
	$error = true;
}

// Display the data 
if(!$error && isset($showRaw) && $showRaw)
	echo $data;
else
	JsonUtil::display($response);
?>