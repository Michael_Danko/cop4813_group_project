<?php

class User extends BaseModel {

	// Set Model data
	protected $primaryKey  = 'USER_ID';
	protected $table       = 'USER';

	/**
	 * Construct the Model while constructing the parent
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Get all of the Model's rows
	 * @return array
	 */
	public function rows(){
		$rows = $this->sql->select()->get();

		return $rows;
	}

	/**
	 * Query the database for the passed username
	 * @param string $username
	 * @return mixed
	 */
	public function getSubByUsername($username){
		$row = $this->sql->select()->where('USERNAME', '=', $username)->findFirst();
		return $row;
	}
}
?>