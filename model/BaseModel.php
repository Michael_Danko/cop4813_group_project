<?php 
abstract class BaseModel {
	
	protected $primaryKey    = 'ID';
	protected $table         = 'TABLE';
	protected $excludeCols   = array();
	protected $attributes    = array();
	protected $sql;
	
	protected $timestampCreatedAt = 'CREATED_AT';
	protected $timestampUpdatedAt = 'UPDATED_AT';

	public $autoIncrement = true;
	public $perPage       = 15;
	public $sortBy;
	public $sortDir       = 'ASC';
	public $timestamps    = false;
	
	public function __construct(){
		global $wpdb;
		
		$this->wpdb    = $wpdb;
		$this->sortBy  = $this->primaryKey;
		$this->sql     = new SQL($this);
		
		// Go ahead and set the attributes
		$this->initAttributes();
	}
	
	/**
	 * Get the Model's primaryKey
	 * @return string
	 */
	public function getPrimaryKey(){
		return $this->primaryKey;
	}
	
	/**
	 * Get the Model's attributes
	 * @return array
	 */
	public function getAttributes(){
		return $this->attributes;
	}

	/**
	 * Get the model's table
	 * @return string
	 */
	public function getTable(){
		return $this->table;
	}
	
	/**
	 * Get the Model's attributes
	 * @return array
	 */
	public function getAttributesNoPK(){
		return $this->filterPKFromAttributes();
	}

	/**
	 * Get the Model's excluded columns
	 * @return array
	 */
	public function getExcludeCols(){
		return $this->excludeCols;
	}
	
	/**
	 * Get the columns that are not excluded with the table name appended and comma delimited
	 * @return string
	 */
	public function getColumnsForSQL(){
		$columns = $this->table . "." . implode("," . $this->table . ".", array_filter(array_keys($this->attributes), array($this, 'excludeColumn')));
		return $columns;	
	}
	
	/**
	 * Get the attribute value
	 * @param mixed $key
	 */
	public function getAttribute($key){
		return $this->attributes[$key];
	}
	
	/**
	 * Set the attribute value
	 * @param mixed $key
	 * @param mixed $value
	 */
	public function setAttribute($key, $value){
		$this->attributes[$key] = $value;
	}
	
	/**
	 * Create a new blank object from the attributes 
	 * @return object
	 */
	public function create(){
		$object = new stdClass();
		if(count($this->attributes) > 0){
			foreach($this->attributes as $key => $value){
				$object->$key = NULL;
			}
		}
		return $object;
	}
	
	/**
	 * Get a single row of the Model passed on the primary key
	 * @param mixed $pk
	 * @return object
	 */
	public function row($pk){
		$object = $this->sql->find($pk);
		
		return $object;
	}
	
	/**
	 * Get all of the Model's rows
	 * @return array
	 */
	public function rows(){
		$rows = $this->sql->select()->orderBy($this->sortBy, $this->sortDir)->get();
		
		return $rows;
	}
	
	/**
	 * Save the model to the DB
	 * Inserts if no primary key is set or updates if it is
	 * @return bool|string
	 */
	public function save(){

		// Check if primary key is a composite key
		if(is_array($this->primaryKey)){
			foreach($this->primaryKey as $key){
				$keyVal[$key] = $this->attributes[$key];
			}
		} else
			$keyVal = $this->attributes[$this->primaryKey];

		// Insert timestamps if model has them
		if($this->timestamps)
			$this->attributes[$this->timestampUpdatedAt] = date('Y-m-d H:i:s');


		// Find the entry for update or insert if not there
		if(!$this->sql->find($keyVal)){

			// Insert timestamps if model has them
			if($this->timestamps)
				$this->attributes[$this->timestampCreatedAt] = date('Y-m-d H:i:s');

			$result = $this->sql->insert();

			// Set the pk to the incremented number, if incremental
			if($result && $this->autoIncrement){
				$this->attributes[$this->primaryKey] = $result;
				$result = true;	
			} elseif($result !== false)
				$result = true;	
			else 
				$result = false;
		} else {
			$result = $this->sql->update();
			if($result !== false)
				$result = true;
			else
				$result = false;
		}

		if(!$result)
			$result = $this->sql->getLastError();

		return $result;
	}

	/**
	 * Delete model(s) from the DB
	 * @param mixed|array $pk
	 * @return int
	 * @throws \Exception
	 */
	public function delete($pk){
		if(!$pk)
			throw new Exception('There is no primary key set to delete model');
		
		$result = $this->sql->delete($pk);
		
		return $result;
	}
	
	/**
	 * Method to sanitize a string for insert as the primary key
	 * @param string $string
	 * @return mixed|string
	 */
	public function sanitizeKey($string = ''){
		$string = trim($string);
		$string = strip_tags($string);
		$string = preg_replace('/[^a-z0-9_-\s]+/i', '', $string);
		$string = strtoupper($string);
		$string = str_replace(array(' ', '-'), array('_', '_'), $string);

		// Check database for key and add increment if found
		$count = 1;
		$incr  = '';
		do {
			$found = $this->sql->find($string . $incr);
			if(!$found)
				$string .= $incr;
			else
				$incr = '_' . ++$count;
		} while($found);

		return $string;
	}
		
	/**
	 * Sort an array of objects based on the Model sort variables
	 * @param array $array
	 * @return array
	 */
	protected function sortList($array){
		$sortBy  = $this->sortBy;
		$sortDir = $this->sortDir;
		usort($array, function($a, $b) use ($sortBy, $sortDir){
			switch($sortDir){
				case 'DESC':
				case 'desc':
					$cmp = strcmp($b->$sortBy, $a->$sortBy);
					break;
				case 'ASC':
				case 'asc':
				default:
					$cmp = strcmp($a->$sortBy, $b->$sortBy);
					break;
			}
			return $cmp;
		});	
		
		return $array;
	}
	
	/**
	 * Check to see if the column is in the excluded array
	 * @param string $columnName 
	 * @return bool
	 */
	protected function excludeColumn($columnName){ 
		return !in_array($columnName, $this->excludeCols);
	}
	
	/**
	 * Set the attributes based on the columns in the database table
	 */
	private function initAttributes(){
		try{
			foreach($this->sql->cols() as $column){
				$this->attributes[$column] = NULL;
			}
		} catch(Exception $e){
			echo $e->__toString();	
		}
	}
	
	/**
	 * Filter the attribute array to not include the primary key
	 * @return array
	 */
	private function filterPKFromAttributes(){
		$attr = $this->attributes;

		// If the primary key is an array
		if(is_array($this->primaryKey)){
			foreach($this->primaryKey as $key)
				unset($this->attributes[$key]);
		} else
			unset($attr[$this->primaryKey]);

		// Go ahead and filter out the created timestamp, if set
		if($this->timestamps)
			unset($attr[$this->timestampCreatedAt]);
		return $attr;
	}
}
?>