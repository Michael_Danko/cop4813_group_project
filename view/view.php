<?php
/**
 * View class that controls the making of the html pages that are displayed
 * Main responsibility is to control what processes are to be ran or displayed.
 * This class uses the ListTable class.
 * 
 * @author Keith Andrews
 */
class View {

	/**
	 * Method for making the view and including the passed file path and data
	 * @param string $path
	 * @param array  $data
	 * @return string
	 */
	public static function make($path, $data = array()){
		$completeFile = WEB_PATH . '/views/' . $path;

		try{
			// Make sure it's there
			if(!file_exists($completeFile))
				throw new \Exception('No file exists for the path "' . $path . '".');
			
			// Extract all of the data array elements into variable for the view
			extract($data, EXTR_PREFIX_SAME, 'mod');		
			include $completeFile;
		} catch(\Exception $e){
			echo $e->getMessage();	
		}

		return;
	}
}